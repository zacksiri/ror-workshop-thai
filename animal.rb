module SpecialAbility
  attr_accessor :ability
end



class Robot
  include SpecialAbility

  def initialize(a_name, a_manufacture_date, a_status, ability)
    @name = a_name
    @manufactured = a_manufacture_date
    @status = a_status
    @ability = ability
  end

  attr_reader :name, :manufactured, :status
end

class Animal
  def initialize(a_name, a_age, a_status)
    @name = a_name
    @age = a_age
    @status = a_status
  end
  #attr_reader :name, :age, :status
  # def name
  #   @name
  # end

  # def age
  #   @age
  # end

  # def status
  #   @status
  # end

  #attr_writer :age, :name, :status
  # def age=(number)
  #   @age = number
  # end
  attr_accessor :age, :name, :status
end

class Mammal < Animal
  def initialize(a_name, a_age, a_status, a_legs)
    super(a_name, a_age, a_status)
    @legs = a_legs
  end
  attr_reader :legs

  def baby_food
    "milk"
  end
end

class Bird < Animal
  include SpecialAbility

  def initialize(a_name, a_age, a_status)
    super(a_name, a_age, a_status)
    @wings = 2
    @legs = 2
    @ability = 'fly'
  end

  attr_reader :wings, :legs

  def baby_food
    "worm"
  end
end
